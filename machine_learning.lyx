#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass revtex4-1
\options rmp, preprint, a4paper, 12pt, tightenlines, notitlepage, nofootinbib, longbibliography, eqsecnum, fleqn
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_author "François Konschelle [fraschelle[at]free[dot]fr]"
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks true
\pdf_pdfborder true
\pdf_colorlinks true
\pdf_backref false
\pdf_pdfusetitle true
\pdf_quoted_options "linkcolor=red, urlcolor=magenta, citecolor=blue"
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine natbib
\cite_engine_type authoryear
\biblio_style apsrmp4-1
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Standard
Machine learning / Apprentissage statistique / Modèlisation Statistique
 de la Science
\end_layout

\begin_layout Section
Science
\end_layout

\begin_layout Section
Supervised learning / Apprentissage supervisé / Régression linéaire
\end_layout

\begin_layout Standard
C'est en fait de la regression linéaire.
\end_layout

\begin_layout Standard
On cherche à obtenir la fonction 
\begin_inset Formula $\Phi\left(X\right)$
\end_inset

 telle que 
\begin_inset Formula 
\[
Y=\Phi\left(X\right)
\]

\end_inset

où l'on ne connaît que les 
\begin_inset Formula $Y$
\end_inset

 (appelées cibles / 
\emph on
target
\emph default
) et les 
\begin_inset Formula $X$
\end_inset

 (appelées données / 
\emph on
data
\emph default
) que l'on va éventuellement retravailler / sélectionner (
\emph on
features
\emph default
 
\begin_inset Formula $\rightarrow$
\end_inset

 
\emph on
information retrieval
\emph default
 / 
\emph on
feature selection
\emph default
).
\end_layout

\begin_layout Standard
Historiquement, c'est de la science expérimentale : comment modéliser les
 mesures obtenues par des expériences sans connaissance à priori ? 
\end_layout

\begin_layout Standard
Tout le processus d'apprentissage est caché dans une 
\series bold
descente de gradient
\series default
 (
\emph on
gradient descent
\emph default
), éventuellement stochastique (
\emph on
stochastic gradient descent
\emph default
) et une 
\series bold
fonction de coût
\series default
.
\end_layout

\begin_layout Standard
Hypothèses : 
\end_layout

\begin_layout Itemize
que 
\begin_inset Formula $\Phi$
\end_inset

 existe (et soit suffisament régulière) au moins en probabilité : 
\begin_inset Formula 
\[
\lim_{N\rightarrow\infty}\left\{ y_{i}\right\} _{N}=Y=\Phi\left(X\right)
\]

\end_inset

suffisament de données permettent de représenter la fonction 
\begin_inset Formula $\Phi$
\end_inset

 exactement
\end_layout

\begin_layout Itemize
que le bruit potentiel caché (latent) dans 
\begin_inset Formula $\left\{ y_{i}\right\} _{N}\sim Y$
\end_inset

 soit identifiable par la fonction de coût
\end_layout

\begin_layout Subsection
Régression linéaire
\end_layout

\begin_layout Subsection
Notations et généralisations
\end_layout

\begin_layout Subsection
Algorithme de Newton-Raphson / Gradient descent
\end_layout

\begin_layout Section
Unsupervised learning / Apprentissage non-supervisé / Modélisation
\end_layout

\begin_layout Standard
C'est en fait de la modélisation statistique (modèle probabiliste), ou du
 traitement du signal (modèle déterministe).
 
\end_layout

\begin_layout Standard
En traitement du signal, on veut calculer un modèle du type 
\begin_inset Formula 
\[
z=\Phi\left[x\right]+B
\]

\end_inset

où 
\begin_inset Formula $\Phi$
\end_inset

 est une relation fonctionnelle de la variable 
\begin_inset Formula $x$
\end_inset

, très souvent linéaire et toujours déterministe, et posée à priori, et
 
\begin_inset Formula $B$
\end_inset

 est un opérateur de bruit.
 On veut en général débruiter (= enlever le bruit 
\begin_inset Formula $B$
\end_inset

 / 
\emph on
denoising
\emph default
) pour ne garder que le modèle 
\begin_inset Formula $\Phi\left[x\right]$
\end_inset

.
 Permet également de synthétiser les données (= les compresser / 
\emph on
compressed sensing
\emph default
) si on trouve une représentation parcimonieuse de 
\begin_inset Formula $\Phi$
\end_inset

 (= avec peu de coefficients).
\end_layout

\begin_layout Standard
Hypothèses : 
\end_layout

\begin_layout Itemize
connaissance à priori de 
\begin_inset Formula $\Phi\left[x\right]$
\end_inset

, qui doit être forte
\end_layout

\begin_layout Itemize
superposition linéaire du bruit, qui doit être faible
\end_layout

\begin_layout Standard
En statistique, on veut représenter les données 
\begin_inset Formula $\left\{ x_{i}\right\} _{N}$
\end_inset

 de la variable aléatoire 
\begin_inset Formula $X$
\end_inset

 par la densité de probabilité 
\begin_inset Formula $p\left(x\right)$
\end_inset

.
 Si on se donne une densité de probabilité à priori 
\begin_inset Formula $p\left(x\right)$
\end_inset

, on peut, par calcul statistique, trouver la meilleure estimation 
\begin_inset Formula $\hat{p}\left(\left\{ x_{i}\right\} _{N}\right)$
\end_inset

 à partir des données.
\end_layout

\begin_layout Standard
Hypothèses : 
\end_layout

\begin_layout Itemize
connaissance à priori de 
\begin_inset Formula $p\left(x\right)$
\end_inset

, qui peut être dirigée par les données fondamentale (exemple de la théorie
 statistique des gaz de Boltzmann)
\end_layout

\begin_layout Itemize
méthode d'estimation (bayésienne / 
\emph on
bayesian update
\emph default
 vs.
 fréquentiste / 
\emph on
maximum likelihood
\emph default
 par exemple) donnée à priori
\end_layout

\begin_layout Section
Différence entre modélisation et régression
\end_layout

\begin_layout Standard
Dans le cadre de la modélisation, on se donne l'ensemble de la fonction
 
\begin_inset Formula $\Phi\left(x\right)$
\end_inset

.
 Dans le cas d'une régression, on ne se donne que la classe de la fonction
 
\begin_inset Formula $\Phi\left(x\right)$
\end_inset

.
\end_layout

\begin_layout Standard
Dit différemment, et pour l'exemple d'une fonction affine 
\begin_inset Formula $\Phi\left(x\right)=\alpha x+\beta$
\end_inset


\end_layout

\begin_layout Itemize
dans le cas de la régression, on 
\series bold
calcule
\series default
 les coefficients 
\begin_inset Formula $\left(\alpha,\beta\right)$
\end_inset

 qui correspondent au mieux à l'hypothèse d'existence de la fonction 
\begin_inset Formula $\Phi\left(x\right)$
\end_inset

, en 
\series bold
proposant
\series default
 une fonction de coût pour le calcul
\end_layout

\begin_layout Itemize
dans le cas de la modélisation, on 
\series bold
propose
\series default
 les coefficients 
\begin_inset Formula $\left(\alpha,\beta\right)$
\end_inset

 à priori, dans le cadre d'une loi statistique (par exemple 
\begin_inset Formula $\alpha$
\end_inset

 et 
\begin_inset Formula $\beta$
\end_inset

 suivent une loi normale)
\end_layout

\begin_layout Section
Reinforcement learning
\end_layout

\begin_layout Standard
Apprentissage par renforcement.
\end_layout

\begin_layout Standard
C'est en fait l'application de la théorie des jeux aux statistiques de l'apprent
issage.
\end_layout

\begin_layout Standard
Un joueur intéragit avec un plateau, ou d'autres joueurs, ou les deux, via
 un ensemble de règles.
 Transposés en apprentissage machine, ces éléments extérieurs deviennent
 un environnement.
\end_layout

\begin_layout Standard
C'est l'approche privilégiée de la robotique, dans sa recherche d'autonomisation
 des robots.
\end_layout

\begin_layout Itemize
une règle du jeu (
\emph on
policy, 
\emph default
les règles stochastiques de comportement du robot)
\end_layout

\begin_layout Itemize
une interaction avec l'environnement (action/réaction 
\begin_inset Formula $\leftrightarrow$
\end_inset

 joueur/joueurs)
\end_layout

\begin_layout Itemize
une récompense ou une pénalisation (
\emph on
reward
\emph default
), qui met à jour la règle du jeu
\end_layout

\begin_layout Section
Représentations mathématiques des statistiques statiques
\end_layout

\begin_layout Subsection
Théorême de Bayes et probabilité conditionnelles
\end_layout

\begin_layout Subsection
Modèles graphiques
\end_layout

\begin_layout Subsection
Théorie quantique / Théorie de jauge / Topologie et géométrie algébrique
\end_layout

\begin_layout Section
Phénomènes dépendant du temps
\end_layout

\begin_layout Standard
Il y a une structure intrinsèque à la donnée, qui est le temps.
 Quand les dés se pipent avec le temps par exemple.
\end_layout

\begin_layout Subsection
Série de Markov, intégrale de chemin
\end_layout

\begin_layout Subsection
Analyse temps-fréquence / quasi-classique
\end_layout

\begin_layout Subsection
Théorie du transport / équations stochastiques
\end_layout

\end_body
\end_document
