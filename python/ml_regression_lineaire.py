#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Présentation des régressions linéaires simples

"""

import numpy as np
import matplotlib.pyplot as plt

def linear_regression(x,y):
    """
    Take two series of floats (ideally, numpy array compatible objects) and returns
    the coefficients alpha and beta (two floats) corresponding to the linear
    regression
        y = alpha + beta * x
    
    """
    x, y = np.array(x), np.array(y)
    xm, ym = np.mean(x), np.mean(y)
    beta = np.sum((x-xm)*(y-ym))/np.sum((x-xm)*(x-xm))
    alpha = ym - beta*xm
    return alpha, beta

# illustration of pseudo-linearisation
N = 25 # sample size
x = 5*np.random.random(N)
x.sort()
epsilon = np.random.randn(N)

y = -x
# generate the y_exp = exp(-x) data
y_exp = np.exp(y)
alpha, beta = linear_regression(x,y_exp)

plt.scatter(x,y_exp)
plt.title("y = f(x)")
plt.ylabel("y")
plt.xlabel("x")
plt.savefig("regression_lineaire_exp_1.pdf")
plt.clf()

plt.scatter(x,y)
plt.plot(x,y_fit,'r',label="{:.4} x + {:.4}".format(beta,alpha))
plt.title("ln(y) = f(x)")
plt.ylabel("ln(y)")
plt.xlabel("x")
plt.legend()
plt.savefig("regression_lineaire_exp_2.pdf")
plt.clf()


# pure linearization
N = 25 # sample size
x = 5*np.random.random(N)
x.sort()
epsilon = np.random.randn(N)

y = 2+3*x+epsilon

alpha, beta = linear_regression(x,y)
y_fit = np.array([alpha+beta*t for t in x])

plt.scatter(x,y)
plt.plot(x,y_fit,'r',label="{:.4} x + {:.4}".format(beta,alpha))
plt.title("y = f(x)")
plt.xlabel("x")
plt.ylabel("y")
plt.legend()
plt.savefig("regression_lineaire_1000.pdf")
plt.clf()

# withdraw the last 5 points
x_new = x[:-5]
y_new = y[:-5]
x_old = x[-5:]
y_old = -18+8*x_old + epsilon[-5:]
alpha, beta = linear_regression(x,y)
y_fit = np.array([alpha+beta*t for t in x])
plt.scatter(x_new,y_new)
# plt.scatter(x_old,y_old,marker='x',c='orange')
plt.scatter(x[-5:],y[-5:],marker='x',c='orange')
plt.plot(x,y_fit,'r',label="{:.4} x + {:.4}".format(beta,alpha))
plt.title("y = f(x)")
plt.xlabel("x")
plt.ylabel("y")
plt.legend()
plt.savefig("regression_lineaire_-5.pdf")
plt.clf()

# jacknife / crossvalidation
for _ in range(4):
    ind_sample = np.random.choice(range(len(x)),
                                  size = int(4*len(x)/5),
                                  replace=False)
    out_sample = np.array([i for i in range(len(x)) if i not in ind_sample])
    x_sample = x[ind_sample]
    y_sample = 2 + 3*x_sample + epsilon[ind_sample]
    x_sample.sort()
    y_sample.sort()
    x_out = x[out_sample]
    y_out = 2 + 3*x_out + epsilon[out_sample]
    x_out.sort()
    y_out.sort()
    alpha, beta = linear_regression(x_sample,y_sample)
    y_fit = np.array([alpha+beta*t for t in x_sample])
    plt.scatter(x_sample,y_sample,marker='o',c='blue')
    plt.scatter(x_out,y_out,marker='x',c='orange')
    plt.plot(x_sample,y_fit,'r',label="{:.4} x + {:.4}".format(beta,alpha))
    plt.title("y = f(x)")
    plt.xlabel("x")
    plt.ylabel("y")
    plt.legend()
    plt.savefig("regression_lineaire_jacknife_"+str(_)+".pdf")
    plt.clf()

all_alphas, all_betas = list(), list()
for _ in range(50):
    ind_sample = np.random.choice(range(len(x)),
                                  size = int(4*len(x)/5),
                                  replace=False)
    out_sample = np.array([i for i in range(len(x)) if i not in ind_sample])
    x_sample = x[ind_sample]
    y_sample = 2 + 3*x_sample + 2*epsilon[ind_sample]
    x_sample.sort()
    y_sample.sort()
    x_out = x[out_sample]
    y_out = 2 + 3*x_out + epsilon[out_sample]
    x_out.sort()
    y_out.sort()
    alpha, beta = linear_regression(x_sample,y_sample)
    y_fit = np.array([alpha+beta*t for t in x_sample])
    all_alphas.append(alpha)
    all_betas.append(beta)
    plt.plot(x_sample,y_fit,'r')
string = "y = ({:.4} +/- {:.2}) x + ({:.4} +/- {:.2})".format(np.mean(all_betas),
                                                              np.std(all_betas),
                                                              np.mean(all_alphas),
                                                              np.std(all_alphas))
plt.scatter(x,y,marker='o',c='blue',label=string)
plt.title("y = f(x)")
plt.xlabel("x")
plt.ylabel("y")
plt.legend()
plt.savefig("regression_lineaire_jacknife_all.pdf")
plt.clf()
    



from sklearn.linear_model import LinearRegression

lr = LinearRegression(fit_intercept=True) # to calculate the alpha

lr.fit(x.reshape(-1,1),y.reshape(-1))

print("I've found a linear plot y = {:.4} x + {:.4}".format(lr.coef_[0],lr.intercept_))

alpha = lr.intercept_
beta = lr.coef_[0]

x_test = [2,4,12]
y_test = lr.predict(np.array(x_test).reshape(-1,1))

for x,y in zip(x_test,y_test):
    string = "from calculation : {:.4}".format(alpha+beta*x)
    string += '\t'+"from sklearn : {:.4}".format(y)
    print(string)

# multi-variable calculation
from sklearn.preprocessing import PolynomialFeatures
    
N = 100
x = np.random.random(N)
epsilon = np.random.randn(N)
xr = x.reshape(-1,1)
X = np.concatenate([xr,xr*xr],axis=1)
y = 2 + 4*x + 35*x*x + epsilon
lr = LinearRegression(fit_intercept=True)
lr.fit(X,y)
lr.coef_
lr.intercept_

T = np.linspace(0,1,num=25)
y_fit = np.array([lr.intercept_ + lr.coef_[0]*t + lr.coef_[1]*t*t for t in T])


plt.scatter(x,y)
plt.plot(T,y_fit,c='red',label="y = {:.2} x^2 + {:.2} x + {:.2}".format(lr.coef_[1],
                                                                         lr.coef_[0],
                                                                         lr.intercept_))
plt.xlabel("x")
plt.ylabel("y")
plt.title("y = f(x)")
plt.legend()
plt.savefig("regression_lineaire_quadratique")
plt.clf()


# working way
x = np.arange(5)
X = x[:,np.newaxis]
poly = PolynomialFeatures(degree=2)
X = poly.fit_transform(X)
y = 2 + 4*x + 35*x*x + epsilon[:5]
lr = LinearRegression(fit_intercept=False)
lr.fit(X,y)
lr.coef_
x_test = 5*np.random.random(5)
X_test = x_test[:,np.newaxis]
y_test = 2 + 4*x_test + 35*x_test*x_test
y_pred = lr.predict(poly.fit_transform(X_test))
np.sum((y_test-y_pred)**2)
