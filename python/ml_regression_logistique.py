#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Exemple de regression logistique

"""

import numpy as np
import matplotlib.pyplot as plt

N = 10
x1 = np.random.random(N)+0.8
x0 = np.random.random(N)
y1 = [1 for _ in range(N)]
y0 = [0 for _ in range(N)]

x = np.concatenate([x0,x1])
y = np.array(y0+y1)

plt.scatter(x,y)

X = x.reshape(-1,1)

from sklearn.linear_model import LogisticRegression

logistic = LogisticRegression(penalty='none')  

logistic.fit(X,y)
logistic.coef_
logistic.intercept_

x_test = np.random.random(N)+0.5
x_test = x_test.reshape(-1,1)
y_pred = logistic.predict(x_test)

x_fit = np.linspace(0,2,25)
y_fit = np.array([1/(1+np.exp(-logistic.coef_[0]*t-logistic.intercept_[0])) for t in x_fit])
plt.scatter(x,y)
plt.plot(x_fit,y_fit,c='r',label="y = (1+exp(-{:.2} x - {:.2}))".format(logistic.coef_[0],
                                                                        logistic.intercept_[0]))

logistic_regression(x,y)
